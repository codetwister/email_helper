package ro.codetwisters.emailhelper.gui;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: codetwister
 * Date: 9/15/12
 * Time: 7:14 PM
 */
public class HelperForm {
    private JPanel panel1;
    private JButton copyHTMLButton;
    private JPanel itemsPanel;
    private JPanel buttonPanel;
    private JLabel previewLabel;
    private JScrollPane itemsScroll;
    private JTabbedPane itemTabs;
    private JPanel donePanel;
    private JPanel inProgressPanel;
    private JPanel todoPanel;
    private JPanel reviewsPanel;
    private JPanel meetingsPanel;
    private JPanel documentationPanel;
    private JPanel notesPanel;

    public JPanel getPanel1() {
        return panel1;
    }

    public JPanel getItemsPanel() {
        return itemsPanel;
    }

    public JButton getCopyHTMLButton() {
        return copyHTMLButton;
    }

    public JLabel getPreviewLabel() {
        return previewLabel;
    }

    public JScrollPane getItemsScroll() {
        return itemsScroll;
    }

    public JPanel getDonePanel() {
        return donePanel;
    }

    public JPanel getInProgressPanel() {
        return inProgressPanel;
    }

    public JPanel getTodoPanel() {
        return todoPanel;
    }

    public JPanel getReviewsPanel() {
        return reviewsPanel;
    }

    public JPanel getMeetingsPanel() {
        return meetingsPanel;
    }

    public JPanel getDocumentationPanel() {
        return documentationPanel;
    }

    public JPanel getNotesPanel() {
        return notesPanel;
    }

    public JTabbedPane getItemTabs() {
        return itemTabs;
    }
}
