package ro.codetwisters.emailhelper.gui;

import ro.codetwisters.emailhelper.models.HelperItem;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/25/13
 * Time: 6:48 PM
 */
public interface HelperItemRenderer<T extends HelperItem> {
    public String getItemHtml(T item);
    public String getItemPreviewText(T item);
    public HelperItemEditorPanel<T> createItemEditorPanel(T item, HelperItemEditorPanel.HelperItemChangedListener listener);
}
