package ro.codetwisters.emailhelper.gui;

import ro.codetwisters.emailhelper.models.HelperItem;
import ro.codetwisters.emailhelper.models.HelperType;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: codetwister
 * Date: 9/15/12
 * Time: 10:07 PM
 */
public class Controller implements ActionListener, ItemPanel.ItemActionsListener {
    private HelperForm helperForm;
    private List<String> groups;
    private SettingsProvider settingsProvider;

    private List<ItemPanel> itemPanelList;
    private ItemPanel currentlyEditing;

    public Controller(HelperForm helperForm, SettingsProvider settingsProvider) {
        this.helperForm = helperForm;
        this.settingsProvider = settingsProvider;

        // add event listeners
        helperForm.getCopyHTMLButton().addActionListener(this);
        // init helper items list
//        EmailHelperService service = ServiceManager.getService(EmailHelperService.class);
//        if (service != null) {
//            helperItemList = service.getHelperItemsList();
//        } else {
//            System.out.println("SERVICE IS NULL");
//        }
        groups = Arrays.asList(settingsProvider.getDefinedGroups().split("\\|+"));
        helperForm.getItemsPanel().setLayout(new BoxLayout(helperForm.getItemsPanel(), BoxLayout.Y_AXIS));
//        helperForm.getItemsPanel().setLayout(new GridLayout(0, 1));
        // preview stuff
//        helperForm.getPreviewLabel().setFont(new Font("Serif", Font.PLAIN, 14));
        helperForm.getPreviewLabel().setFont(new Font("Serif", Font.PLAIN, 14));

        itemPanelList = new ArrayList<ItemPanel>();

        if (itemPanelList.size() == 0) {
            addMinutesItem(HelperType.SIMPLE, groups.get(0));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(helperForm.getCopyHTMLButton())) {
            // copy html
            String content = getAllItemsHtml(false);
            Toolkit.getDefaultToolkit().getSystemClipboard()
                    .setContents(new StringSelection(content), null);
        }
    }

    public void addMinutesItem(HelperType type, String group) {
        // validate input fields and add item to the list
        HelperItem item = HelperItem.createHelperItem(type, group);
        ItemPanel itemPanel = new ItemPanel(item, groups, this);
        ItemPanel prevPanel = itemPanelList.size()>0?itemPanelList.get(itemPanelList.size()-1):null;
        itemPanel.setPrevPanel(prevPanel);
        if (prevPanel != null) prevPanel.setNextPanel(itemPanel);
        itemPanelList.add(itemPanel);
        updateMinutesItemsList();
    }

    @Override
    public SettingsProvider getSettingsProvider() {
        return settingsProvider;
    }

    @Override
    public void updateCurrentEditingPanel(ItemPanel itemPanel) {
        if (currentlyEditing != null) {
            currentlyEditing.requestPreview();
        }
        currentlyEditing = itemPanel;
    }

    @Override
    public void removeItem(ItemPanel itemPanel) {
        ItemPanel prevPanel = itemPanel.getPrevPanel();
        ItemPanel nextPanel = itemPanel.getNextPanel();
        if (prevPanel != null) prevPanel.setNextPanel(nextPanel);
        if (nextPanel != null) nextPanel.setPrevPanel(prevPanel);
        itemPanelList.remove(itemPanel);
        updateMinutesItemsList();
    }

    private void updateMinutesItemsList() {
        helperForm.getItemsPanel().removeAll();
        for (ItemPanel itemPanel : itemPanelList) {
            helperForm.getItemsPanel().add(itemPanel);
        }

        helperForm.getItemsPanel().revalidate();
        updatePreview();
    }

    private String getAllItemsHtml(boolean wrapHtml) {
        StringBuilder sb = new StringBuilder();
        if (wrapHtml) sb.append("<html>");
        sb.append("<ul>");

        for (String group : groups) {
            sb.append(getHtmlForOneGroup(group));
        }

        sb.append("</ul>");
        if (wrapHtml) sb.append("</html>");
        return sb.toString();
    }

    public String getHtmlForOneGroup(String group) {
        // create a new list without empty items
        ArrayList<HelperItem> newList = new ArrayList<HelperItem>();
        for (ItemPanel itemPanel : itemPanelList) {
            if (itemPanel.getItem().getGroup().equals(group)) {
                newList.add(itemPanel.getItem());
            }
        }
        if (newList.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<li><span style=\"font-weight:bold;\">").append(group).append(": </span></li><ul>");
        for (HelperItem helperItem : newList) {
            sb.append("<li>");
            sb.append(helperItem.getType().renderer.getItemHtml(helperItem));
            sb.append("</li>");
        }
        sb.append("</ul>");
        return sb.toString();
    }

    public void updatePreview() {
        // construct preview
        helperForm.getPreviewLabel().setText(getAllItemsHtml(true));
    }

}
