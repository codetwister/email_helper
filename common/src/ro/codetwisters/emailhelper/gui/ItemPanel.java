package ro.codetwisters.emailhelper.gui;

import ro.codetwisters.emailhelper.gui.renderers.GenericPreviewPanel;
import ro.codetwisters.emailhelper.models.HelperItem;
import ro.codetwisters.emailhelper.models.HelperType;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/25/13
 * Time: 7:22 PM
 */
public class ItemPanel extends JPanel implements GenericPreviewPanel.Editor, HelperItemEditorPanel.HelperItemChangedListener {

    private HelperItem item;
    private List<String> groups;

    private GenericPreviewPanel preview;
    private HelperItemEditorPanel editor; // this needs to be set to null when not in use.

    private ItemPanel nextPanel;
    private ItemPanel prevPanel;

    private ItemActionsListener itemActionsListener;

    public ItemPanel(HelperItem item, List<String> groups, ItemActionsListener itemActionsListener) {
        this.item = item;
        this.groups = groups;
        this.itemActionsListener = itemActionsListener;
        setLayout(new BorderLayout());
        preview = new GenericPreviewPanel(this);
        requestPreview();
    }

    public HelperItem getItem() {
        return item;
    }

    public void setNextPanel(ItemPanel nextPanel) {
        this.nextPanel = nextPanel;
    }

    public void setPrevPanel(ItemPanel prevPanel) {
        this.prevPanel = prevPanel;
    }

    public ItemPanel getNextPanel() {
        return nextPanel;
    }

    public ItemPanel getPrevPanel() {
        return prevPanel;
    }

    @Override
    public void requestEditor(String focusedField) {
        itemActionsListener.updateCurrentEditingPanel(this);
        // remove the preview panel and add the editor in place
        if (editor == null) {
            editor = item.getType().renderer.createItemEditorPanel(item, this);
        }
        remove(preview);
        add(editor, BorderLayout.CENTER);
        editor.focusField(focusedField);
        revalidate();
        repaint();
    }

    @Override
    public void requestPreview() {
        if (editor != null) {
            remove(editor);
        }
        preview.setPreviewText(item.getType().renderer.getItemPreviewText(item));
        add(preview, BorderLayout.CENTER);
        revalidate();
        repaint();
        itemActionsListener.updatePreview();
    }

    @Override
    public void itemTypeChanged(HelperType newType) {
        item = HelperItem.createHelperItem(newType, item.getGroup());
        requestPreview();
        editor = null;
        requestEditor(null);
    }

    @Override
    public void deleteItem() {
        itemActionsListener.removeItem(this);
    }

    @Override
    public void moveItemUp() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void moveItemDown() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void itemDoneEditing() {
        requestPreview();
    }

    @Override
    public void focusNextItem(String field) {
        if (nextPanel == null) {
            // need to create another panel
            itemActionsListener.addMinutesItem(item.getType(), item.getGroup());
        }
        requestPreview();
        nextPanel.requestEditor(field);
    }

    @Override
    public void focusPreviousItem(String field) {
        if (prevPanel != null) {
            requestPreview();
            prevPanel.requestEditor(field);
        }
    }

    @Override
    public List<String> getGroups() {
        return groups;
    }

    @Override
    public SettingsProvider getSettingsProvider() {
        return itemActionsListener.getSettingsProvider();
    }

    public interface ItemActionsListener {
        public void updateCurrentEditingPanel(ItemPanel itemPanel);
        public void removeItem(ItemPanel itemPanel);
        public void addMinutesItem(HelperType type, String group);
        public void updatePreview();
        public SettingsProvider getSettingsProvider();
    }
}
