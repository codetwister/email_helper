package ro.codetwisters.emailhelper.gui.impl;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;
import ro.codetwisters.emailhelper.models.impl.TicketItem;
import ro.codetwisters.emailhelper.settings.SettingsProvider;
import ro.codetwisters.emailhelper.tickets.TicketHelperProvider;
import ro.codetwisters.emailhelper.tickets.TicketUpdatedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/18/12
 * Time: 5:41 PM
 */
public class TicketItemEditorPanel extends HelperItemEditorPanel<TicketItem> implements KeyListener, TicketUpdatedListener {

    private JTextField ticketIdField;
    private JTextField descriptionField;
    private JButton fetchDescButton;

    public TicketItemEditorPanel(HelperItemChangedListener listener, TicketItem helperItem) {
        super(listener, helperItem);
    }

    @Override
    public JPanel createDetails() {
        JPanel panel = new JPanel(new BorderLayout(10, 10));
        JPanel idAndRefresh = new JPanel(new BorderLayout(10, 10));
        ticketIdField = new JTextField(10);
        descriptionField = new JTextField(17);
        ticketIdField.addKeyListener(this);
        descriptionField.addKeyListener(this);

        fetchDescButton = new JButton(new ImageIcon(getClass().getResource("refresh_icon.png")));
        fetchDescButton.addActionListener(this);
        fetchDescButton.setBorder(null);
        fetchDescButton.setBorderPainted(false);
        fetchDescButton.setContentAreaFilled(false);
        fetchDescButton.setOpaque(false);

        idAndRefresh.add(ticketIdField, BorderLayout.WEST);
        idAndRefresh.add(fetchDescButton, BorderLayout.CENTER);
        panel.add(idAndRefresh, BorderLayout.WEST);
        panel.add(descriptionField, BorderLayout.CENTER);
        ticketIdField.setText(helperItem.getTicketId());
        descriptionField.setText(helperItem.getDescription());
        return panel;
    }

    @Override
    public void updatePanels() {
        //To change body of implemented methods use File | Settings | File Templates.
        ticketIdField.setText(helperItem.getTicketId());
        descriptionField.setText(helperItem.getDescription());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(fetchDescButton)) {
            TicketHelperProvider.getInstance(helperItem.getType()).fetchDescriptionAsync(
                    helperItem,
                    listener.getSettingsProvider(),
                    this
            );
        } else {
            super.actionPerformed(e);
        }
    }

    @Override
    public void focusField(String field) {
        if (field != null && field.equals("ticket")) {
            ticketIdField.requestFocus();
        } else {
            descriptionField.requestFocus();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do here
    }

    @Override
    public void keyPressed(KeyEvent e) {
        String focusField = e.getSource().equals(descriptionField)?"desc":"ticket";
        if (e.getKeyCode() == 10 || e.getKeyCode() == 40) {
            listener.focusNextItem(focusField);
        } else if (e.getKeyCode() == 38) {
            listener.focusPreviousItem(focusField);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        helperItem.setTicketId(ticketIdField.getText());
        helperItem.setDescription(descriptionField.getText());
    }

    @Override
    public void ticketUpdated(TicketItem item) {
        helperItem.setDescription(item.getDescription());
        helperItem.setRemoteTicketId(item.getRemoteTicketId());
        helperItem.setTicketId(item.getTicketId());
        helperItem.setDescription(item.getDescription());
        helperItem.setTicketUrl(item.getTicketUrl());
        helperItem.setGroup(item.getGroup());
        updatePanels();
    }
}
