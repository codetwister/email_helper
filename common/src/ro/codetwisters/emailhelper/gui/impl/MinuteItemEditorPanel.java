package ro.codetwisters.emailhelper.gui.impl;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;
import ro.codetwisters.emailhelper.models.impl.MinuteItem;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/18/12
 * Time: 1:36 AM
 */
public class MinuteItemEditorPanel extends HelperItemEditorPanel<MinuteItem> implements KeyListener {

    private JTextField personField;
    private JTextField descriptionField;

    public MinuteItemEditorPanel(HelperItemChangedListener listener, MinuteItem helperItem) {
        super(listener, helperItem);
    }

    @Override
    public JPanel createDetails() {
        JPanel panel = new JPanel(new BorderLayout(10, 10));
        personField = new JTextField(10);
        descriptionField = new JTextField(17);
        personField.addKeyListener(this);
        descriptionField.addKeyListener(this);
        panel.add(personField, BorderLayout.WEST);
        panel.add(descriptionField, BorderLayout.CENTER);
        personField.setText(helperItem.getPerson());
        descriptionField.setText(helperItem.getDetails());
        return panel;
    }

    @Override
    public void updatePanels() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void focusField(String field) {
        if (field != null && field.equals("pers")) {
            personField.requestFocus();
        } else {
            descriptionField.requestFocus();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do here
    }

    @Override
    public void keyPressed(KeyEvent e) {
        String focusField = e.getSource().equals(descriptionField)?"desc":"pers";
        if (e.getKeyCode() == 10 || e.getKeyCode() == 40) {
            listener.focusNextItem(focusField);
        } else if (e.getKeyCode() == 38) {
            listener.focusPreviousItem(focusField);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        helperItem.setPerson(personField.getText());
        helperItem.setDetails(descriptionField.getText());
    }
}
