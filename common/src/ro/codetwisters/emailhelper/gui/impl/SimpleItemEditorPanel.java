package ro.codetwisters.emailhelper.gui.impl;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;
import ro.codetwisters.emailhelper.models.impl.SimpleItem;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/17/12
 * Time: 6:58 PM
 */
public class SimpleItemEditorPanel extends HelperItemEditorPanel<SimpleItem> implements KeyListener {

    private JTextField simpleTextInput;

    public SimpleItemEditorPanel(HelperItemChangedListener listener, SimpleItem helperItem) {
        super(listener, helperItem);
    }

    @Override
    public JPanel createDetails() {
        JPanel panel = new JPanel(new BorderLayout());
        simpleTextInput = new JTextField(17);
        simpleTextInput.addKeyListener(this);
        panel.add(simpleTextInput);
        simpleTextInput.setText(helperItem.getText());
        return panel;
    }

    @Override
    public void updatePanels() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void focusField(String field) {
        simpleTextInput.requestFocus();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // nothing to do here
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 10 || e.getKeyCode() == 40) {
            listener.focusNextItem(null);
        } else if (e.getKeyCode() == 38) {
            listener.focusPreviousItem(null);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        helperItem.setText(simpleTextInput.getText());
    }
}
