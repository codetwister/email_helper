package ro.codetwisters.emailhelper.gui;

import ro.codetwisters.emailhelper.models.HelperItem;
import ro.codetwisters.emailhelper.models.HelperType;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: codetwister
 * Date: 9/16/12
 * Time: 4:21 PM
 */
public abstract class HelperItemEditorPanel<T extends HelperItem> extends JPanel implements ActionListener, FocusListener {

    protected JComboBox itemType;
    protected JComboBox groupCombo;
    protected JButton deleteButton;
    protected JButton doneButton;
    protected JButton upButton;
    protected JButton downButton;
    protected T helperItem;
    protected HelperItemChangedListener listener;

    public HelperItemEditorPanel(HelperItemChangedListener listener, T helperItem) {
        this.listener = listener;
        this.helperItem = helperItem;
        setLayout(new BorderLayout(10, 0));
        addComponents();
    }

    private void addComponents() {
        itemType = new JComboBox();

        add(itemType, BorderLayout.WEST);
        JPanel inputsPanel = new JPanel(new BorderLayout(10, 0));
        groupCombo = new JComboBox();
        inputsPanel.add(groupCombo, BorderLayout.WEST);

        // setup item type combobox
        for (HelperType type : HelperType.values()) {
            itemType.addItem(type);
        }
        itemType.setSelectedItem(helperItem.getType());
        itemType.addFocusListener(this);

        // setup group combo box
        for (String group : listener.getGroups()) {
            groupCombo.addItem(group);
        }
        groupCombo.setSelectedItem(helperItem.getGroup());
        groupCombo.addFocusListener(this);

        add(inputsPanel, BorderLayout.CENTER);

        deleteButton = new JButton("delete");
        deleteButton.addActionListener(this);
        doneButton = new JButton("done");
        doneButton.addActionListener(this);
        upButton = new JButton("up");
        upButton.addActionListener(this);
        downButton = new JButton("down");
        downButton.addActionListener(this);


        JPanel buttonsPanel = new JPanel(new FlowLayout());
        buttonsPanel.add(doneButton);
        buttonsPanel.add(upButton);
        buttonsPanel.add(downButton);
        buttonsPanel.add(deleteButton);

        add(buttonsPanel, BorderLayout.EAST);

        inputsPanel.add(createDetails(), BorderLayout.CENTER);
    }

    public abstract JPanel createDetails();
    public abstract void updatePanels();

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(deleteButton)) {
            listener.deleteItem();
        } else if (e.getSource().equals(upButton)) {
            listener.moveItemUp();
        } else if (e.getSource().equals(downButton)) {
            listener.moveItemDown();
        } else if (e.getSource().equals(doneButton)) {
            listener.itemDoneEditing();
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        // nothing to do here
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (e.getSource().equals(itemType)) {
            listener.itemTypeChanged((HelperType) itemType.getSelectedItem());
        } else if (e.getSource().equals(groupCombo)) {
            helperItem.setGroup((String) groupCombo.getSelectedItem());
        }
    }

    public abstract void focusField(String field);

    public interface HelperItemChangedListener {
        public void itemTypeChanged(HelperType newType);
        public void deleteItem();
        public void moveItemUp();
        public void moveItemDown();
        public void itemDoneEditing();
        public void focusNextItem(String field);
        public void focusPreviousItem(String field);
        public List<String> getGroups();
        public SettingsProvider getSettingsProvider();
    }
}
