package ro.codetwisters.emailhelper.gui.renderers;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;
import ro.codetwisters.emailhelper.gui.HelperItemRenderer;
import ro.codetwisters.emailhelper.gui.impl.MinuteItemEditorPanel;
import ro.codetwisters.emailhelper.models.HelperType;
import ro.codetwisters.emailhelper.models.impl.MinuteItem;
import ro.codetwisters.emailhelper.models.impl.SimpleItem;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/25/13
 * Time: 6:57 PM
 */
public class MinuteItemRenderer implements HelperItemRenderer<MinuteItem> {

    private static final MinuteItemRenderer instance = new MinuteItemRenderer();

    public static MinuteItemRenderer getInstance() {
        return instance;
    }

    @Override
    public String getItemHtml(MinuteItem item) {
        String color = "black";
        if (item.getType() == HelperType.DECISION) {
            color = "green";
        } else if (item.getType() == HelperType.ACTION) {
            color = "red";
        }
        return "<span style=\"color:"+color+";\"><strong>["+item.getPerson()+"]</strong> "+item.getDetails()+"</span>";
    }

    @Override
    public String getItemPreviewText(MinuteItem item) {
        return "["+item.getPerson()+"] "+item.getDetails();
    }

    @Override
    public HelperItemEditorPanel<MinuteItem> createItemEditorPanel(MinuteItem item, HelperItemEditorPanel.HelperItemChangedListener listener) {
        return new MinuteItemEditorPanel(listener, item);
    }
}
