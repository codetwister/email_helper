package ro.codetwisters.emailhelper.gui.renderers;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/25/13
 * Time: 7:40 PM
 */
public class GenericPreviewPanel extends JPanel implements MouseListener {

    private JLabel textPreviewLabel;
    private Editor editor;

    public GenericPreviewPanel(Editor editor) {
        this.editor = editor;
        textPreviewLabel = new JLabel();
        textPreviewLabel.setText("Preview");
        setLayout(new BorderLayout());
        add(textPreviewLabel, BorderLayout.CENTER);
        this.addMouseListener(this);
    }

    public void setPreviewText(String preview) {
        textPreviewLabel.setText(preview);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        editor.requestEditor(null);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public interface Editor {
        public void requestEditor(String focusedField);
        public void requestPreview();
    }
}
