package ro.codetwisters.emailhelper.gui.renderers;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;
import ro.codetwisters.emailhelper.gui.HelperItemRenderer;
import ro.codetwisters.emailhelper.gui.impl.SimpleItemEditorPanel;
import ro.codetwisters.emailhelper.models.impl.SimpleItem;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/25/13
 * Time: 6:57 PM
 */
public class SimpleItemRenderer implements HelperItemRenderer<SimpleItem> {

    @Override
    public String getItemHtml(SimpleItem item) {
        return item.getText();
    }

    @Override
    public String getItemPreviewText(SimpleItem item) {
        String itemText= item.getText();
        if (itemText == null || itemText.equals("")) {
            itemText = "Not available";
        }
        return itemText;
    }

    @Override
    public HelperItemEditorPanel<SimpleItem> createItemEditorPanel(SimpleItem item, HelperItemEditorPanel.HelperItemChangedListener listener) {
        return new SimpleItemEditorPanel(listener, item);
    }
}
