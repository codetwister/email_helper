package ro.codetwisters.emailhelper.gui.renderers;

import ro.codetwisters.emailhelper.gui.HelperItemEditorPanel;
import ro.codetwisters.emailhelper.gui.HelperItemRenderer;
import ro.codetwisters.emailhelper.gui.impl.TicketItemEditorPanel;
import ro.codetwisters.emailhelper.models.impl.SimpleItem;
import ro.codetwisters.emailhelper.models.impl.TicketItem;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/25/13
 * Time: 6:57 PM
 */
public class TicketItemRenderer implements HelperItemRenderer<TicketItem> {

    private static final TicketItemRenderer instance = new TicketItemRenderer();

    public static TicketItemRenderer getInstance() {
        return instance;
    }

    @Override
    public String getItemHtml(TicketItem item) {
        return "<strong>[<a href=\""+item.getTicketUrl()+"\">"+item.getTicketId()+"</a>]</strong> "+item.getDescription();
    }

    @Override
    public String getItemPreviewText(TicketItem item) {
        return "["+item.getTicketId()+"] "+item.getDescription();
    }

    @Override
    public HelperItemEditorPanel<TicketItem> createItemEditorPanel(TicketItem item, HelperItemEditorPanel.HelperItemChangedListener listener) {
        return new TicketItemEditorPanel(listener, item);
    }
}
