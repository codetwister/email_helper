package ro.codetwisters.emailhelper.gui;

import javax.swing.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/27/12
 * Time: 6:24 PM
 */
public class HelperSettingsForm {
    private JPanel mainPanel;
    private JTextField groupsField;
    private JTextField jiraUserField;
    private JPasswordField jiraPasswordField;
    private JTextField jiraBaseUrlField;


    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JTextField getGroupsField() {
        return groupsField;
    }

    public JTextField getJiraUserField() {
        return jiraUserField;
    }

    public JPasswordField getJiraPasswordField() {
        return jiraPasswordField;
    }

    public JTextField getJiraBaseUrlField() {
        return jiraBaseUrlField;
    }
}
