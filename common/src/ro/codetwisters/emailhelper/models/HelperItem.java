package ro.codetwisters.emailhelper.models;

import ro.codetwisters.emailhelper.models.impl.TicketItem;
import ro.codetwisters.emailhelper.models.impl.MinuteItem;
import ro.codetwisters.emailhelper.models.impl.SimpleItem;

/**
 * Created with IntelliJ IDEA.
 * User: codetwister
 * Date: 9/15/12
 * Time: 10:59 PM
 * Generic helper item
 */
public abstract class HelperItem {

    private String group;
    protected HelperType type;

    protected HelperItem(HelperType type, String group) {
        this.type = type;
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public HelperType getType() {
        return type;
    }

    public static HelperItem createHelperItem(HelperType type, String group) {
        switch (type) {
            case SIMPLE:
                return new SimpleItem(type, group);
            case INFO:
            case DECISION:
            case ACTION:
                return new MinuteItem(type, group);
            case JIRA:
            case RALLY:
                return new TicketItem(type, group);
        }
        return null;
    }

}
