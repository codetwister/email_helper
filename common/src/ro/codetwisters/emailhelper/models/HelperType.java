package ro.codetwisters.emailhelper.models;

import ro.codetwisters.emailhelper.gui.HelperItemRenderer;
import ro.codetwisters.emailhelper.gui.renderers.MinuteItemRenderer;
import ro.codetwisters.emailhelper.gui.renderers.SimpleItemRenderer;
import ro.codetwisters.emailhelper.gui.renderers.TicketItemRenderer;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/17/12
 * Time: 6:12 PM
 */
public enum HelperType {
    SIMPLE("Simple text", new SimpleItemRenderer()),
    INFO("Information", MinuteItemRenderer.getInstance()),
    DECISION("Decision", MinuteItemRenderer.getInstance()),
    ACTION("Action item", MinuteItemRenderer.getInstance()),
    JIRA("JIRA", TicketItemRenderer.getInstance()),
    RALLY("Rally", TicketItemRenderer.getInstance());

    public String typeDesc;
    public HelperItemRenderer renderer;

    private HelperType(String typeDesc, HelperItemRenderer renderer) {
        this.typeDesc = typeDesc;
        this.renderer = renderer;
    }

    @Override
    public String toString() {
        return typeDesc;
    }
}
