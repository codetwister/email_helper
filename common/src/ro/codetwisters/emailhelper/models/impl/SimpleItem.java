package ro.codetwisters.emailhelper.models.impl;

import org.json.JSONException;
import org.json.JSONObject;
import ro.codetwisters.emailhelper.models.HelperItem;
import ro.codetwisters.emailhelper.models.HelperType;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/17/12
 * Time: 6:46 PM
 */
public class SimpleItem extends HelperItem {

    private String text = "";

    public SimpleItem(HelperType type, String group) {
        super(type, group);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
