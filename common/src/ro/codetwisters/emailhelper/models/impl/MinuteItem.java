package ro.codetwisters.emailhelper.models.impl;

import org.json.JSONException;
import org.json.JSONObject;
import ro.codetwisters.emailhelper.models.HelperItem;
import ro.codetwisters.emailhelper.models.HelperType;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/17/12
 * Time: 6:47 PM
 */
public class MinuteItem extends HelperItem {

    private String person = "";
    private String details = "";

    public MinuteItem(HelperType type, String group) {
        super(type, group);
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPerson() {
        return person;
    }

    public String getDetails() {
        return details;
    }

}
