package ro.codetwisters.emailhelper.models.impl;

import org.json.JSONException;
import org.json.JSONObject;
import ro.codetwisters.emailhelper.models.HelperItem;
import ro.codetwisters.emailhelper.models.HelperType;
import ro.codetwisters.emailhelper.settings.SettingsProvider;
import ro.codetwisters.emailhelper.tickets.TicketHelper;
import ro.codetwisters.emailhelper.tickets.TicketHelperProvider;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/18/12
 * Time: 5:42 PM
 */
public class TicketItem extends HelperItem {

    private String ticketId;
    private String description;
    private long remoteTicketId; // this is used by rally, but possibly by other systems as well
    private String ticketUrl;

    public TicketItem(HelperType type, String group) {
        super(type, group);
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setRemoteTicketId(long remoteTicketId) {
        this.remoteTicketId = remoteTicketId;
    }

    public long getRemoteTicketId() {
        return remoteTicketId;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }
}
