package ro.codetwisters.emailhelper.settings;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/18/13
 * Time: 6:08 PM
 */
public class RallySettings {
    private String user;
    private String pass;

    public RallySettings(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }
}
