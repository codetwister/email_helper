package ro.codetwisters.emailhelper.settings;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/15/13
 * Time: 7:21 PM
 */
public interface SettingsProvider {

    public void loadSettings();
    public void saveSettings();

    // GROUP SETTINGS
    public String getDefinedGroups();
    public void setDefinedGroups(String groups);

    // JIRA SETTINGS
    public JiraSettings getJiraSettings();
    public void setJiraSettings(JiraSettings settings);

    public RallySettings getRallySettings();
    public void setRallySettings(RallySettings rallySettings);

    // TODO check if other settings need to be saved

}
