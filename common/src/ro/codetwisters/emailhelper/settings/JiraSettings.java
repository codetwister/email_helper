package ro.codetwisters.emailhelper.settings;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/15/13
 * Time: 7:23 PM
 */
public class JiraSettings {

    private String baseUrl;
    private String userName;
    private String password;

    public JiraSettings(String baseUrl, String userName, String password) {
        this.baseUrl = baseUrl;
        this.userName = userName;
        this.password = password;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
