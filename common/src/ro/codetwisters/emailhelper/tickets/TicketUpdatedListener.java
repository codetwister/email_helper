package ro.codetwisters.emailhelper.tickets;

import ro.codetwisters.emailhelper.models.impl.TicketItem;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 8/22/13
 * Time: 7:08 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TicketUpdatedListener {
    public void ticketUpdated(TicketItem item);
}
