package ro.codetwisters.emailhelper.tickets;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import ro.codetwisters.emailhelper.models.impl.TicketItem;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/20/12
 * Time: 11:54 AM
 */
public class JiraHelper implements TicketHelper {

    @Override
    public String getTicketUrl(TicketItem ticketItem, SettingsProvider settingsProvider) {
        return settingsProvider.getJiraSettings().getBaseUrl()+"browse/"+ticketItem.getTicketId();
    }

    @Override
    public void fetchDescriptionAsync(final TicketItem item, final SettingsProvider settingsProvider, final TicketUpdatedListener listener) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("FETCH JIRA DESCRIPTION");

                        DefaultHttpClient client = new DefaultHttpClient();
                            client.getCredentialsProvider().setCredentials(
                                    new AuthScope(AuthScope.ANY),
                                    new UsernamePasswordCredentials(
                                            settingsProvider.getJiraSettings().getUserName(),
                                            settingsProvider.getJiraSettings().getPassword()
                                    ));

                        HttpGet request = new HttpGet(settingsProvider.getJiraSettings().getBaseUrl()+"rest/api/latest/issue/"+item.getTicketId());
                        try {
                            HttpResponse response = client.execute(request);
                            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            JSONObject jsonResp = new JSONObject(sb.toString());
                            JSONObject fields = jsonResp.getJSONObject("fields");
                            final String desc = fields.optString("summary");
                            item.setDescription(desc);
                            item.setTicketUrl(settingsProvider.getJiraSettings().getBaseUrl()+"browse/"+item.getTicketId());
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    listener.ticketUpdated(item);
                                }
                            });
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).start();
    }

}
