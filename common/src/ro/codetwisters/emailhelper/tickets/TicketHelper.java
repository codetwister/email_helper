package ro.codetwisters.emailhelper.tickets;

import ro.codetwisters.emailhelper.models.impl.TicketItem;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/20/12
 * Time: 11:04 AM
 */
public interface TicketHelper {
    public String getTicketUrl(TicketItem ticketItem, SettingsProvider settingsProvider);
    public void fetchDescriptionAsync(TicketItem item, SettingsProvider settingsProvider, TicketUpdatedListener listener);
}
