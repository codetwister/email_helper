package ro.codetwisters.emailhelper.tickets;

import ro.codetwisters.emailhelper.models.HelperType;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/18/12
 * Time: 5:50 PM
 */
public class TicketHelperProvider {

    public static TicketHelper jiraInstance = new JiraHelper();

    public static TicketHelper getInstance(HelperType type) {
        switch (type) {
            case JIRA: return jiraInstance;
        }
        return null;
    }
}
