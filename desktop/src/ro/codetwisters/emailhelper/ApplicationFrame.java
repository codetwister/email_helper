package ro.codetwisters.emailhelper;

import ro.codetwisters.emailhelper.gui.Controller;
import ro.codetwisters.emailhelper.gui.HelperForm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/15/13
 * Time: 12:10 PM
 */
public class ApplicationFrame extends JFrame {

    public ApplicationFrame() throws HeadlessException {
        super("Email Helper");

        //basic setup
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setDefaultLookAndFeelDecorated(true);

        //setup menu
        JMenuItem settingsItem = new JMenuItem("Settings");
        settingsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SettingsDialog dialog = new SettingsDialog(ApplicationFrame.this);
                dialog.pack();
                dialog.setVisible(true);
            }
        });

        JMenuBar menuBar = new JMenuBar();

        menuBar.add(new JMenu("File"));
        menuBar.add(settingsItem);
        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(new JMenu("Help"));

        setJMenuBar(menuBar);

        HelperForm helperForm = new HelperForm();
        new Controller(helperForm, new DesktopSettingsProvider());
        setContentPane(helperForm.getPanel1());

        // show UI
        setMinimumSize(new Dimension(300, 300));
        setVisible(true);
        setExtendedState(MAXIMIZED_BOTH);
    }

}
