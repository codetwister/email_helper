package ro.codetwisters.emailhelper;

import ro.codetwisters.emailhelper.gui.HelperSettingsForm;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/15/13
 * Time: 2:20 PM
 */
public class SettingsDialog extends JDialog {

    public SettingsDialog(Frame owner) {
        super(owner);
        setTitle("Settings");

        HelperSettingsForm settingsForm = new HelperSettingsForm();

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPanel.add(settingsForm.getMainPanel(), BorderLayout.CENTER);

        JPanel buttonsPanel = new JPanel(new FlowLayout());
        buttonsPanel.add(new JButton("Apply"));
        buttonsPanel.add(new JButton("Cancel"));

        mainPanel.add(buttonsPanel, BorderLayout.SOUTH);

        setContentPane(mainPanel);
    }

}
