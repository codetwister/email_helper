package ro.codetwisters.emailhelper;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/15/13
 * Time: 12:08 PM
 */
public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ApplicationFrame();
            }
        });
    }

}
