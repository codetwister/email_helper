package ro.codetwisters.emailhelper;

import ro.codetwisters.emailhelper.settings.JiraSettings;
import ro.codetwisters.emailhelper.settings.RallySettings;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

/**
 * Created with IntelliJ IDEA.
 * User: mihalynagy
 * Date: 4/16/13
 * Time: 5:30 PM
 */
public class DesktopSettingsProvider implements SettingsProvider {

    @Override
    public String getDefinedGroups() {
        return "Done|In progress|To Do|Reviews|Meetings|Documentation|Notes/Issues|Project update";
    }

    @Override
    public void setDefinedGroups(String groups) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JiraSettings getJiraSettings() {
        return new JiraSettings("http://jira.corp.pinger.com/", "mihalynagy", "");
    }

    @Override
    public void setJiraSettings(JiraSettings settings) {

    }

    @Override
    public RallySettings getRallySettings() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setRallySettings(RallySettings rallySettings) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void loadSettings() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void saveSettings() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
