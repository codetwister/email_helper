package ro.codetwisters.emailhelper;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import ro.codetwisters.emailhelper.gui.Controller;
import ro.codetwisters.emailhelper.gui.HelperForm;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 11/14/12
 * Time: 3:29 PM
 */
public class HelperToolWindowFactory implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(Project project, ToolWindow toolWindow) {
        HelperForm helperForm = new HelperForm();
        // TODO create settings provider for plugin
        new Controller(helperForm, new DummySettingsProvider());
        toolWindow.getComponent().add(helperForm.getPanel1());
    }
}
