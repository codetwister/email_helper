package ro.codetwisters.emailhelper;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import org.jetbrains.annotations.Nls;
import ro.codetwisters.emailhelper.gui.HelperSettingsForm;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: codetwister
 * Date: 10/23/12
 * Time: 1:33 AM
 */
public class HelperConfigurable implements Configurable {

    private HelperSettingsForm settingsForm;
    private String jiraBaseUrl;
    private String jiraPassword;
    private String jiraUser;
    private String groupsCsv;

    @Nls
    @Override
    public String getDisplayName() {
        return "SoftVision Email Helper";
    }

    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public JComponent createComponent() {
        settingsForm = new HelperSettingsForm();

        jiraBaseUrl = PropertiesComponent.getInstance().getValue("email.helper.jira.baseUrl", "");
        jiraUser = PropertiesComponent.getInstance().getValue("email.helper.jira.user", "");
        jiraPassword = PropertiesComponent.getInstance().getValue("email.helper.jira.password", "");
        groupsCsv = PropertiesComponent.getInstance().getValue("email.helper.groups", "Project update|Done|In Progress|To Do|Reviews|Meetings|Documentation|Notes|Other");

        settingsForm.getGroupsField().setText(groupsCsv);
        settingsForm.getJiraBaseUrlField().setText(jiraBaseUrl);
        settingsForm.getJiraUserField().setText(jiraUser);
        settingsForm.getJiraPasswordField().setText(jiraPassword);

        return settingsForm.getMainPanel();
    }

    @Override
    public boolean isModified() {
        return this.groupsCsv != settingsForm.getGroupsField().getText() ||
                this.jiraBaseUrl != settingsForm.getJiraBaseUrlField().getText() ||
                this.jiraPassword != new String(settingsForm.getJiraPasswordField().getPassword()) ||
                this.jiraUser == settingsForm.getJiraUserField().getText();
    }

    @Override
    public void apply() throws ConfigurationException {
        this.jiraPassword = new String(settingsForm.getJiraPasswordField().getPassword());
        this.jiraUser = settingsForm.getJiraUserField().getText();
        this.jiraBaseUrl = settingsForm.getJiraBaseUrlField().getText();
        this.groupsCsv = settingsForm.getGroupsField().getText();
        //save to persistence
        PropertiesComponent.getInstance().setValue("email.helper.groups", groupsCsv);
        PropertiesComponent.getInstance().setValue("email.helper.jira.baseUrl", jiraBaseUrl);
        PropertiesComponent.getInstance().setValue("email.helper.jira.user", jiraUser);
        PropertiesComponent.getInstance().setValue("email.helper.jira.password", jiraPassword);
    }

    @Override
    public void reset() {
        settingsForm.getJiraBaseUrlField().setText(jiraBaseUrl);
        settingsForm.getJiraPasswordField().setText(jiraPassword);
        settingsForm.getJiraUserField().setText(jiraUser);
        settingsForm.getGroupsField().setText(groupsCsv);
    }

    @Override
    public void disposeUIResources() {
        settingsForm = null;
    }
}
