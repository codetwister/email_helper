package ro.codetwisters.emailhelper;

import com.intellij.ide.util.PropertiesComponent;
import ro.codetwisters.emailhelper.settings.JiraSettings;
import ro.codetwisters.emailhelper.settings.RallySettings;
import ro.codetwisters.emailhelper.settings.SettingsProvider;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 8/26/13
 * Time: 12:00 PM
 */
public class DummySettingsProvider implements SettingsProvider {
    @Override
    public void loadSettings() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void saveSettings() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getDefinedGroups() {
        return PropertiesComponent.getInstance().getValue("email.helper.groups", "Done|In Progress|To Do|Reviews|Meetings|Documentation|Notes|Other|Project update");
    }

    @Override
    public void setDefinedGroups(String groups) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JiraSettings getJiraSettings() {
        return new JiraSettings(
                PropertiesComponent.getInstance().getValue("email.helper.jira.baseUrl", ""),
                PropertiesComponent.getInstance().getValue("email.helper.jira.user", ""),
                PropertiesComponent.getInstance().getValue("email.helper.jira.pass", "")
        );
    }

    @Override
    public void setJiraSettings(JiraSettings settings) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public RallySettings getRallySettings() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setRallySettings(RallySettings rallySettings) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
