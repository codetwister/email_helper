package ro.codetwisters.emailhelper;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurableProvider;

/**
 * User: codetwister
 * Date: 10/23/12
 * Time: 1:32 AM
 */
public class HelperConfigurationProvider extends ConfigurableProvider {

    @Override
    public Configurable createConfigurable() {
        return new HelperConfigurable();
    }
}
