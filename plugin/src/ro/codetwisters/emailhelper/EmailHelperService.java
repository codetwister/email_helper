package ro.codetwisters.emailhelper;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.components.StoragePathMacros;
import org.jetbrains.annotations.Nullable;
import ro.codetwisters.emailhelper.models.HelperItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mihaly.nagy
 * Date: 12/27/12
 * Time: 8:28 PM
 */
@State(
        name = "SVEmailHelperConfiguration",
        storages = @Storage(id = "default", file = StoragePathMacros.APP_CONFIG+"/sv_helper.xml")
)
public class EmailHelperService implements PersistentStateComponent<EmailHelperService.State> {

    private List<HelperItem> items;

    @Nullable
    @Override
    public State getState() {
        List<String> serializedItems = new ArrayList<String>();
        for (HelperItem helperItem : items) {
        }
        return new State(serializedItems);
    }

    @Override
    public void loadState(State state) {
        items = new ArrayList<HelperItem>();
        if (state.helperItems != null) {
            for (String itemString : state.helperItems) {
                // TODO
//                items.add(HelperItem.createFromJson(itemString));
            }
        }
    }

    public class State {
        private List<String> helperItems;

        public State() {
            // do nothing
        }

        public State(List<String> helperItems) {
            this.helperItems = helperItems;
        }
    }

    public List<HelperItem> getHelperItemsList() {
        if (items == null) {
            items = new ArrayList<HelperItem>();
        }
        return items;
    }
}
